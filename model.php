<?php
		$doc = new DOMdocument();		//create a DOM for load the file
		$doc->load('SkierLogs.xml');
		$xPath = new DOMXpath($doc);
		
		$node = NULL;
		$list = array();
		$list2= array();
		$db= NULL;
		
		try {
			$db = new PDO('mysql:dbname = skierlogs;host=localhost;charset=utf8mb4', 'root', '');
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch (PDOException $e)
			{
				echo $e->getMessage();
			}
			
		echo "LIST OVER SKIER<br><br>";
		$skiers= $xPath->query('/SkierLogs/Skiers/Skier');	//path
		foreach($skiers as $skier) {
				
				//get userName for skier
				$skierUserName =$skier->getAttribute('userName');
				
				//get first name for skier
				$node=$skier->getElementsByTagName('FirstName');
				$skierFirst=$node->item(0)->textContent;
				
				//get last name for skier
				$node=$skier->getElementsByTagName('LastName');
				$skierLast=$node->item(0)->textContent;
				
				//get Year Of Birth for skier
				$node=$skier->getElementsByTagName('YearOfBirth');
				$skierYear=$node->item(0)->textContent;
				
				//get club for skier
				
				$list= $skierUserName.' '.$skierFirst.' '.$skierLast.' '.$skierYear."\n";
				echo "<br>Skier: ";
		print_r($list);
		echo "<br>";
		}
		$node= NULL;
		echo "<br>LISTE OVER CLUB<br>";
		$clubs=	$xPath->query('/SkierLogs/Clubs/Club');
		foreach($clubs as $club) {
			
			//get id for club
			$clubId =$club->getAttribute('id');
			
			//get name for club
			$node=$club->getElementsByTagName('Name');
			$clubName=$node->item(0)->textContent;
			
			//get city for club
			$node=$club->getElementsByTagName('City');
			$clubCity=$node->item(0)->textContent;
			
			//get county for club
			$node = $club->getElementsByTagName('County');
			$clubCounty=$node->item(0)->textContent;
			
			$list= $clubId.' '.$clubName.' '.$clubCity.' '.$clubCounty."\n";
			
			echo "<br>Club: ";
			print_r($list);
			echo "<br>";
		}
		
		echo "LIST OVER SEASON<br>";
		$seasons=$xPath->query('/SkierLogs/Season');
		$seasonClubs=$xPath->query('/SkierLogs/Season/Skiers');
		$seasonUsers=$xPath->query('/SkierLogs/Season/Skiers/Skier');
		$seasonDistances=$xPath->query('/SkierLogs/Season/Skiers/Skier/Log/Entry');
		foreach($seasons as $season){
			//get season year
			$seasonFallYear=$season->getAttribute('fallYear');
			$list=$seasonFallYear."\n";
			
			echo "season: ";
			print_r ($list);
			echo "<br>";
			
			foreach($seasonClubs as $seasonClub) {
				//get season club
				$seasonClubId=$seasonClub->getAttribute('clubId');
				$list=$seasonClubId."\n";
				
				echo "Club: ";
				print_r ($list);
				echo "<br>";
					foreach($seasonUsers as $seasonUser)
					{
						//get username
						unset($list2);
						$seasonUserName=$seasonUser->getAttribute('userName');
						$list=$seasonUserName."\n";
						echo"Username: ";
						print_r ($list); 
						echo "<br>";
					
							//get distance
							foreach($seasonDistances as $seasonDistance) {
								$node=$seasonDistance->getElementsByTagName('Distance');
								$distance=$node->item(0)->textContent;
								$list2[]=$distance."\n";
							
					
							}
							$totaltDistance=array_sum($list2);
							echo"Distance: ";
								print_r($totaltDistance);
								echo "<br>";
							
					}
			}
		}
		$doc->save('SkierLogs.xml');
			
?>